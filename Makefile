PROJ = threefish
PIN_DEF = ice40hx8k-evb.pcf
DEVICE = hx8k
MODULE = top

all: $(PROJ).bin

%.blif %.json: %.v
	PROJ_NAME=$(PROJ) TOP_VERILOG=$(PROJ).v TOP_MODULE=$(MODULE) yosys synth.tcl
%.json: %.blif

%.asc: $(PIN_DEF) %.blif
	nextpnr-ice40 --hx8k --json $(PROJ).json --pcf $(PIN_DEF) --asc $(PROJ).asc

gui: $(PIN_DEF) $(PROJ).blif
	nextpnr-ice40 --hx8k --json $(PROJ).json --pcf $(PIN_DEF) --asc $(PROJ).asc --gui

%.bin: %.asc
	icepack $< $@

prog: $(PROJ).bin
	sudo iceprogduino $<

sudo-prog: $(PROJ).bin
	@echo 'Executing prog as root!!!'
	sudo iceprogduino $<

clean:
	rm -f $(PROJ).blif $(PROJ).asc $(PROJ).bin $(PROJ).rpt $(PROJ).pad.bin $(PROJ).json
	rm -f $(PROJ).out $(PROJ).vcd

pad: $(PROJ).bin
	tr '\0' '\377' < /dev/zero | dd bs=2k count=1024 of=$(PROJ).pad.bin
	dd if=$(PROJ).bin conv=notrunc of=$(PROJ).pad.bin

%.out: %.v %.tb.v
	iverilog -o $(PROJ).out $^ -DVCD_FILENAME=\"$(PROJ).vcd\"

%.vcd: %.out
	vvp -N ./$<

test: $(PROJ).vcd
	@echo consider starting GTKWave to view this file.

gtkwave: $(PROJ).vcd
	gtkwave -f $<

.PHONY: all pad clean
