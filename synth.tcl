yosys read_verilog $::env(TOP_VERILOG)
yosys synth_ice40 -top $::env(TOP_MODULE)
yosys write_json $::env(PROJ_NAME).json
