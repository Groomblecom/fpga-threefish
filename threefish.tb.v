module top_tb;
	reg clk;
	
	top dut(
		.CLK (clk)
	);

	initial
	begin
		clk = 0;
	end

	always
		#1	clk = !clk;

	initial
	begin
		$dumpfile(`VCD_FILENAME);
		$dumpvars;
	end

	initial
		#1000000	$finish;

endmodule
